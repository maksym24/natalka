var sp_plan = 1;
//////////////////////
var oferta_type = 0;
var oferta_accepted = false;
//////////////////////
var sp_terms = '<div style="padding:2px"><span style="font-size:16px">Условия активации ПАКЕТА УСЛУГ</span><br/><br/>1. ПАКЕТА УСЛУГ активируется на срок указанный в описание ПАКЕТА УСЛУГ и равен: 3 месяца для пакета "Базовый" и 12 месяцев для пакета "Активный"<br/>2. ПАКЕТА УСЛУГ оплачивается полностью на весь срок действия посредством отправки смс сообщения<br/>3. Стоимость одного смс сообщения указана в разделе "Стоимость смс сообщения"<br/>4. При недостаточном, для отправки смс сообщения, балансе оператор связи на своё усмотрение может предложить воспользоваться услугой "смс в кредит", подробнее об услуге "смс в кредит" вы можете узнать у своего оператора связи<br/>5. Отправка смс сообщения доступна только для абонентов России<br/>6. Полученный в ответном смс сообщении КОД, позволяет активировать только один ПАКЕТА УСЛУГ<br/>7. ПАКЕТА УСЛУГ считается активированным, после того, как сайт принял смс КОД и ваши регистрационные данные<br/>8. Воспользоваться ПАКЕТОМ УСЛУГ возможно только после его активации<br/>9. ДОПОЛНИТЕЛЬНЫЙ ПАКЕТ УСЛУГ (ДОПОЛНИТЕЛЬНЫЙ ПРЕМИУМ АККАУНТ).<br/>Вы можете получить еще один пакет услуг совершенно бесплатно, при условии активации ранее аналогичного ПАКЕТА УСЛУГ посредством отправки смс сообщения. Для получения инструкции по активации дополнительного пакета услуг, обащайтесь в <a target="_black" href="/support.html">Техническую поддержку</a></div>';

var sp_data = new Array(2);
sp_data[0] = new Array();
sp_data[1] = new Array();

sp_data[0]['title'] = 'Активный';
sp_data[0]['mos'] = '12 месяцев';
sp_data[0]['pays'] = '0,68';
sp_data[0]['pays_'] = '295 рублей / 12 месяцев';
sp_data[0]['pre'] = '1';
sp_data[0]['num'] = '1017';
sp_data[0]['cost'] = '<div style="padding: 3px; background: #FFFFFF;" id="cost_1616"><style>TABLE.cost td{width:200px;color:#666666;font-size:10px; border-bottom:1px solid #CCCCCC; padding:2px 0px} TABLE.cost td.cost{width:50px;} TABLE.cost td.nb{border-bottom:0px}</style><table class="cost" cellspacing="0" cellpadding="0" border="0"><tr style="border:none"><td colspan="2" class="nb" style="font-size:16px">Стоимость отправки смс сообщения на номер 1017<br/><br/></td></tr><tr><td>ОАО "МегаФон"</td><td class="cost">300,00</td></tr><tr><td>ОАО "МТС"</td><td class="cost">258,31</td></tr><tr><td>ОАО "ВымпелКом" (Билайн)</td><td class="cost">254,24</td></tr><tr><td>ЗАО "НСС"</td><td class="cost">254,24</td></tr><tr><td>ЗАО &laquo;Ростовская Сотовая Связь&raquo; (Теле-2)</td><td class="cost">211,86</td></tr><tr><td>ООО "Контент Урал" (ОАО "Уралсвязьинформ") Utel</td><td class="cost">260,17</td></tr><tr><td>ЗАО &laquo;Астрахань GSM&raquo;</td><td class="cost">254,24</td></tr><tr><td>ЗАО "БайкалВестКом"</td><td class="cost">254,24</td></tr><tr><td>ЗАО "СМАРТС"</td><td class="cost">254,24</td></tr><tr><td>ЗАО "СМАРТС" - Волгоград GSM</td><td class="cost">254,24</td></tr><tr><td>ЗАО "СМАРТС" - Оренбург GSM</td><td class="cost">254,24</td></tr><tr><td>ЗАО "СМАРТС" - Шупашкар GSM</td><td class="cost">254,24</td></tr><tr><td>ЗАО "СМАРТС" - Ярославль GSM</td><td class="cost">254,24</td></tr><tr><td>ЗАО Енисейтелеком (ООО Мобилфон)</td><td class="cost">254,24</td></tr><tr><td>ОАО Алтайсвязь (ООО Мобилфон)</td><td class="cost">254,24</td></tr><tr><td>ЗАО "СтеК Джи Эс Эм" (ООО Мобилфон)</td><td class="cost">254,24</td></tr><tr><td>ЗАО &laquo;Дельта Телеком&raquo; SKYLINK</td><td class="cost">260,00</td></tr><tr><td>(Скай линк) Московская Сотовая Связь</td><td class="cost">260,00</td></tr><tr><td>ЗАО "Акос"</td><td class="cost">256,00</td></tr><tr style="border:none"><td colspan="2" class="nb">Стоимость указана в рублях без НДС</td></tr>';
sp_data[0]['other_country'] = 'Для жителей УКРАИНЫ следует отправлять SMS сообщение на номер: 9050 (стоимость 41.67 гривны без НДС).\\nДля жителей КАЗАХСТАНА следует отправлять SMS сообщение на номер: 1517 (стоимость 528.57 тенге без НДС)';
sp_data[0]['cost_text'] = 'Стоимость услуги от 211,86 до 300 рублей без НДС в зависимости<br/> от вашего оператора связи на основании';

sp_data[1]['title'] = 'Базовый';
sp_data[1]['mos'] = '3 месяца';
sp_data[1]['pays'] = '1,19';
sp_data[1]['pays_'] = '130 рублей / 3 месяца';
sp_data[1]['pre'] = '2';
sp_data[1]['num'] = '1005';
sp_data[1]['cost'] = '<div style="padding: 3px; background: #FFFFFF;" id="cost_1717"><style>TABLE.cost td{width:200px;color:#666666;font-size:10px; border-bottom:1px solid #CCCCCC; padding:2px 0px} TABLE.cost td.cost{width:50px;} TABLE.cost td.nb{border-bottom:0px}</style><table class="cost" cellspacing="0" cellpadding="0" border="0"><tr style="border:none"><td colspan="2" class="nb" style="font-size:16px">Стоимость отправки смс сообщения на номер 1005<br/><br/></td></tr><tr><td>ОАО "МегаФон"</td><td class="cost">114,41</td></tr><tr><td>ОАО "МТС"</td><td class="cost">114,51</td></tr><tr><td>ОАО "ВымпелКом" (Билайн)</td><td class="cost">118,64</td></tr><tr><td>ЗАО "НСС"</td><td class="cost">112,48</td></tr><tr><td>ЗАО &laquo;Ростовская Сотовая Связь&raquo; (Теле-2)</td><td class="cost">114,41</td></tr><tr><td>ООО "Контент Урал" (ОАО "Уралсвязьинформ") Utel</td><td class="cost">90,68</td></tr><tr><td>ЗАО &laquo;Астрахань GSM&raquo;</td><td class="cost">114,41</td></tr><tr><td>ЗАО "БайкалВестКом"</td><td class="cost">114,41</td></tr><tr><td>ЗАО "СМАРТС"</td><td class="cost">114,41</td></tr><tr><td>ЗАО "СМАРТС" - Волгоград GSM</td><td class="cost">114,41</td></tr><tr><td>ЗАО "СМАРТС" - Оренбург GSM</td><td class="cost">114,41</td></tr><tr><td>ЗАО "СМАРТС" - Шупашкар GSM</td><td class="cost">114,41</td></tr><tr><td>ЗАО "СМАРТС" - Ярославль GSM</td><td class="cost">114,41</td></tr><tr><td>ЗАО Енисейтелеком (ООО Мобилфон)</td><td class="cost">114,41</td></tr><tr><td>ОАО Алтайсвязь (ООО Мобилфон)</td><td class="cost">114,41</td></tr><tr><td>ЗАО "СтеК Джи Эс Эм" (ООО Мобилфон)</td><td class="cost">114,41</td></tr><tr><td>ЗАО &laquo;Дельта Телеком&raquo; SKYLINK</td><td class="cost">114,41</td></tr><tr><td>(Скай линк) Московская Сотовая Связь</td><td class="cost">114,41</td></tr><tr><td>ЗАО "Акос"</td><td class="cost">115,26</td></tr><tr style="border:none"><td colspan="2" class="nb">Стоимость указана в рублях без НДС</td></tr>';
sp_data[1]['other_country'] = 'Для жителей УКРАИНЫ следует отправлять SMS сообщение на номер: 9030 (стоимость 25.00 гривны без НДС).\\nДля жителей КАЗАХСТАНА следует отправлять SMS сообщение на номер: 1420 (стоимость 396.43 тенге без НДС)';
sp_data[1]['cost_text'] = 'Стоимость услуги от 90,68 до 118,64 рублей без НДС в зависимости<br/> от вашего оператора связи на основании';



function SelectPackage(type)
{
	if (type) init();
	addSelectPackageWindow();
	//////////////////////
	$('#agree_label').click(function(){$('input[name="agree"]').attr("checked", "checked");});
	if (oferta_accepted == true) 
	{
		$('input[name="agree"]').attr("checked", "checked");
	}
	//////////////////////
	if ($(window).height() < 560) 
	{
		$('.sp_top').css({'height': '0px', 'display': 'none'});
		$('#sp_home').css({'height': '409px'});
	}
	else
	{
		$('.sp_top').css({'height': '173px', 'display': 'block'});
		$('#sp_home').css({'height': '582px'});
	}
	window_open('#auth');
	select_package(1);
}

function addSelectPackageWindow()
{
	$('#popwindow').html(
'<div style="width:704px" id="auth" class="window">'+
'<div style="padding:10px; background:#c3db85">'+
'	<div style="border:1px solid #c2c2c2; padding:1px; background:#FFFFFF">'+
'<div style="height:582px" id="sp_home">'+
'<div class="sp_top"><div>'+sp_rcount+'+</div></div>'+
'<div class="sp_center">'+
'<div class="sp_center_b1">Выберите пакет, который поможет вам в достижении ваших целей!<span style="margin-top:30px">Полный доступ к <strong>'+sp_rcount+'+</strong> студенческих работ</span><span style="height:46px;">'+sp_data[0]['mos']+' доступа</span><span style="height:50px;">'+sp_data[1]['mos']+' доступа</span><style>span.sp_bonus a:hover{text-decoration:underline}</style><div class="sp_bonus_min"><a style="color:#333333; text-decoration: none; font-weight:normal" href="javascript:oferta_type = 1; isAgree()">Бесплатный пакет</a></div></div>'+
'<a id="sp1" href="javascript:select_package(1)" class="sp_a"><span class="sp_title">'+sp_data[0]['title']+'</span><span class="sp_about"><span style="font-weight:bold; font-size:14px">'+sp_data[0]['mos']+'</span> безлимитного<br/>доступа всего от</span><span class="sp_cost"><strong>'+sp_data[0]['pays']+'</strong> рубля в день</span><span class="sp_cost2">(Отсутствует автоматическое продление)</span><span id="sbutton1" class="sp_select_noactive"></span></a>'+
'<a id="sp2" href="javascript:select_package(2)" class="sp_a"><span class="sp_title">'+sp_data[1]['title']+'</span><span class="sp_about"><span style="font-weight:bold; font-size:14px">'+sp_data[1]['mos']+'</span> безлимитного<br/>доступа всего от</span><span class="sp_cost"><strong>'+sp_data[1]['pays']+'</strong> рубля в день</span><span class="sp_cost2">(Отсутствует автоматическое продление)</span><span id="sbutton2" class="sp_select_noactive"></span></a>'+
'</div><div style="clear:right"></div>'+
'    <div style="height:30px" id="sp_footerExchange">'+
'<style>div.sp_f a:hover{text-decoration:none}</style><div style="margin-left:196px" class="sp_f"><input type="checkbox" class="checkbox" name="agree" value="1" id="agree" /><label id="agree_label"> Согласен с условиями </label><a style="" target="_blank" href="/oferta.html">оферты</a></div>'+
'        <a class="sp_button" href="javascript:void(0)" onclick="window_close()"></a>'+
'        <a class="sp_button_green" href="javascript:void(0)" onclick="oferta_type = 2; isAgree()"></a>'+
'        <div id="sp_sp_ajax_div2"></div><div style="clear:right"></div>'+
'    </div>'+
'</div>'+
'    </div>'+
'</div>'+
'	</div>'+
'');	
}


////////////////////////////

function isAgree()
{
var imgProps=$('input[name="agree"]').attr('checked');
//imgProps = true;
if (imgProps) {oferta_accepted = true; oferta();} else {alert('Вы должны принять условия оферты');oferta_accepted = false;}
}
						

function oferta()
{
	if (oferta_accepted == false)
	{
		addOfertaWindow();
		window_open('#auth');
	}
	else
	{
		oferta_ok();
	}
	
}

function oferta_ok()
{
	if (oferta_type == 0) window_close();
	if (oferta_type == 1) exchangePaper_();
	if (oferta_type == 2) packageSelected();
	oferta_accepted = true;
}

function addOfertaWindow()
{
	$('#popwindow').html(
'<div style="width:704px" id="auth" class="window">'+
'<div style="padding:10px; background:#c3db85">'+
'	<div style="border:1px solid #c2c2c2; padding:1px; background:#FFFFFF">'+
'<div style="height:348px" id="sp_home">'+
'<div style="padding:7px 0px 0px 7px">'+
'<strong style="font-size:16px">Прочитайте текст данной публичной оферты</strong>&nbsp;&nbsp;<span style="line-height:20px; font-size: 11px;">(<a style="line-height:20px; font-size: 11px;" target="_blank" href="/oferta.html">в новом окне</a>)</span><br/><br/><iframe style="background-color:#FFFFFF; width:660px; height:250px" src="/oferta.html?lite=1" />'+
''+
'    <div style="height:30px; border:none; padding-top:10px" id="sp_footerExchange">'+
'        <a class="sp_button" href="javascript:void(0)" onclick="window_close()"></a>'+
'        <a class="sp_button_3" href="javascript:void(0)" onclick="oferta_ok()"></a>'+
'    </div>'+
'</div>'+
'</div>'+
'    </div>'+
'</div>'+
'	</div>'+
'');	
}

////////////////////////////

function packageSelected()
{
	addPackageSelectedWindow();
	if ($(window).height() < 560) 
	{
		$('.sp_top').css({'height': '0px', 'display': 'none'});
		$('#sp_home').css({'height': '348px'});
	}
	else
	{
		$('.sp_top').css({'height': '173px', 'display': 'block'});
		$('#sp_home').css({'height': '521px'});
	}
	window_open('#auth');
	
	////
	
	$('#form_package').ajaxForm({
		beforeSubmit: function(a,f,o) {
			o.dataType = "json";
			$('#sp_ajax_div2').addClass('sp_ajax-loading');
			$('#sp_ajax_div2').html('<span id="loadTitle" style="color:#007BBB">Идет загрузка, подождите &nbsp;&nbsp;&nbsp;&nbsp;</span>');
			$('span#loadTitle').animate({opacity:0},0);
			$('span#loadTitle').animate({opacity:1},1500);
		},
		success: function(data) {
			$('#sp_ajax_div2').removeClass('sp_ajax-loading');
			$('#sp_ajax_div2').html('');
			
			if (data.list[0] == "1")
				{
					packageActivate(data.list[1]);
				}
			if (data.list[0] != "1")$('#sp_ajax_div2').html(data.list[1].replace('[n]', '\r\n').replace('[n]', '\r\n'));
		}
	});
}

function packageTest()
{
$('#form_package').trigger('submit');
}

function packageActivate(code)
{
	addPackageActivateWindow(code);
	if ($(window).height() < 560) 
	{
		$('.sp_top').css({'height': '0px', 'display': 'none'});
		$('#sp_home').css({'height': '348px'});
	}
	else
	{
		$('.sp_top').css({'height': '173px', 'display': 'block'});
		$('#sp_home').css({'height': '521px'});
	}
	window_open('#auth');
	////
	
	$('#form_package_activate').ajaxForm({
		beforeSubmit: function(a,f,o) {
			o.dataType = "json";
			$('#sp_ajax_div2').addClass('sp_ajax-loading');
			$('#sp_ajax_div2').html('<span id="loadTitle" style="color:#007BBB">Идет загрузка, подождите &nbsp;&nbsp;&nbsp;&nbsp;</span>');
			$('span#loadTitle').animate({opacity:0},0);
			$('span#loadTitle').animate({opacity:1},1500);
		},
		success: function(data) {
			$('#sp_ajax_div2').removeClass('sp_ajax-loading');
			$('#sp_ajax_div2').html('');
			
			if (data.list[0] == "1")
				{
					window.location.reload(true);
					//alert(1);
				}
			if (data.list[0] != "1")$('#sp_ajax_div2').html(data.list[1].replace('[n]', '\r\n').replace('[n]', '\r\n'));
		}
	});
}

function packageActivateTest()
{
$('#form_package_activate').trigger('submit');
}

function addPackageActivateWindow(code)
{
	$('#popwindow').html(
'<div style="width:704px" id="auth" class="window">'+
'<div style="padding:10px; background:#c3db85">'+
'	<div style="border:1px solid #c2c2c2; padding:1px; background:#FFFFFF">'+
'<div style="height:521px" id="sp_home">'+
'<div class="sp_top"><div>'+sp_rcount+'+</div></div>'+
'<div class="sp_center_activate2">'+
'<div class="sp_center2_left"><span class="spt">Вы выбрали пакет&nbsp;&nbsp;<strong>'+sp_data[sp_plan-1]['title']+'</strong></span>Код <span class="spc">'+code+'</span> принят!<br/><br/>Вам необходимо завести учетную запись пользователя:'+
''+
'<div class="spsms_box"><form id="form_package_activate" action="/ajax/newUser/" enctype="multipart/form-data" method="post" class="spform"><input type="hidden" value="'+code+'" name="code" /><input type="hidden" value="'+sp_plan+'" name="type" /><span class="spsms2">Заведение учетной записи необходимо для дальнейшего доступа к сайту с любого компьютера!</span><div class="spformt">E-mail:</div><div class="spformi"><input type="text" value="" name="mail" maxlength="32"/></div><div style="clear:left"></div><div class="spformt">Пароль:</div><div class="spformi"><input type="password" value="" name="password" maxlength="32"/></div></form></div> '+
'<div style="clear:left"></div></div>'+
'<div class="sp_center2_right">'+
''+
''+
''+
''+
'</div>'+
'</div><div style="clear:left"></div>'+
'    <div style="height:30px" id="sp_footerExchange">'+
'<div class="sp_f"></div>'+
'        <a class="sp_button" href="javascript:void(0)" onclick="window_close()"></a>'+
'        <a class="sp_button_green" href="javascript:void(0)" onclick="packageActivateTest()"></a>'+
'        <div id="sp_ajax_div2"></div><div style="clear:right"></div>'+
'    </div>'+
'</div>'+
'    </div>'+
'</div>'+
'	</div>'+
'');	
}

function addPackageSelectedWindow()
{
	$('#popwindow').html(
'<div style="width:704px" id="auth" class="window">'+
'<div style="padding:10px; background:#c3db85">'+
'	<div style="border:1px solid #c2c2c2; padding:1px; background:#FFFFFF">'+
'<div style="height:521px" id="sp_home">'+
'<div class="sp_top"><div>'+sp_rcount+'+</div></div>'+
'<div class="sp_center_activate">'+
'<div class="sp_center2_left"><span class="spt">Вы выбрали пакет&nbsp;&nbsp;<strong>'+sp_data[sp_plan-1]['title']+'</strong></span>'+sp_data[sp_plan-1]['mos']+' доступа от <span class="spc">'+sp_data[sp_plan-1]['pays']+'</span> рубля в день'+
'<div class="spsms_box"><span style="font-size:11px; color:silver; float:right; display:block; margin:5px 12px 0px 0px;">Для России (<a style="font-size:11px; color:silver;" href="javascript:alert(\''+sp_data[sp_plan-1]['other_country']+'\')">изменить</a>)</span><span class="spsms">Отправьте SMS сообщение</span>с текстом&nbsp;&nbsp;<span class="spsmstext">REF'+sp_data[sp_plan-1]['pre']+'</span>&nbsp;&nbsp;на номер&nbsp;&nbsp;<span class="spsmsnumber">'+sp_data[sp_plan-1]['num']+'</span></div><div style="clear:right"></div>'+
'<div class="spsms_box"><form id="form_package" action="/ajax/newUser/" enctype="multipart/form-data" method="post" class="spform"><span class="spsms2">В течение 1 минуты Вы получите ответное SMS с кодом,</br/>который необходимо ввести в поле ниже:</span><div class="spsmsb1">SMS-код:</div><div class="spsmsb2"><input type="text" value="" name="code" maxlength="6"/></div><div class="spsmsb3">код вводится</br/>латинскими буквами!</div></form></div>'+
'<div style="clear:left"></div></div>'+
'<div class="sp_center2_right">'+
'<div id="sp_info"><div id="sp_info1"><a href="javascript:closeSpInfo()">закрыть</a></div><div id="sp_info2"></div></div><div id="sp_info_plus"><div class="sp_plus">Вы получаете мгновенный доступ к более чем <strong>'+sp_rcount+'</strong> документов</div><div class="sp_plus">Вы можете написать свои работы <strong style="font-size:11px">лучше</strong>, быстрее и получить более высокие оценки!</div><div class="sp_plus"><strong style="font-size:11px">Отсутствует автоматическое продление</strong> на очередной период действия пакета</div><div class="sp_plus_bonus" style="color:#B4B4B4">'+sp_data[sp_plan-1]['cost_text']+' <a style="color:#B4B4B4; font-size:11px;" href="javascript:openSpInfo(1)">cтоимость смс сообщения</a></div></div>'+
'<div class="sp_info"><a target="_black" href="/support.html">Техническая поддержка</a><br/><a target="_blank" href="/oferta.html">Публичная оферта</a><br/><a href="javascript:openSpInfo(1)">Стоимость смс сообщения</a></div>'+
''+
''+
''+
''+
'</div>'+
'</div><div style="clear:left"></div>'+
'    <div style="height:30px" id="sp_footerExchange">'+
'<div class="sp_f"><a class="sp_ffree" href="javascript:SelectPackage(false)">Назад</a></div>'+
'        <a class="sp_button" href="javascript:void(0)" onclick="window_close()"></a>'+
'        <a class="sp_button_green" href="javascript:void(0)" onclick="packageTest()"></a>'+
'        <div id="sp_ajax_div2"></div><div style="clear:right"></div>'+
'    </div>'+
'</div>'+
'    </div>'+
'</div>'+
'	</div>'+
'');	
}

function closeSpInfo()
{
$('#sp_info_plus').css({'display':'block'});
$('#sp_info').css({'display':'none'});
}

function openSpInfo(n)
{
$('#sp_info_plus').css({'display':'none'});
$('#sp_info').css({'display':'block'});
$('#sp_info2').html('');
if (n == 0) $('#sp_info2').html(sp_terms);
if (n == 1) $('#sp_info2').html(sp_data[sp_plan-1]['cost']);
}


function select_package(n)
{
	sp_plan = n;
	if (sp_plan == 1)
	{
		$('#sp1').removeClass('sp_active');
		$('#sp1').addClass('sp_active');
		$('#sp2').removeClass('sp_active');
		$('#sbutton1').removeClass('sp_select_noactive');
		$('#sbutton1').removeClass('sp_select_active');
		$('#sbutton1').addClass('sp_select_active');
		$('#sbutton2').removeClass('sp_select_noactive');
		$('#sbutton2').removeClass('sp_select_active');
		$('#sbutton2').addClass('sp_select_noactive');
	}
	else
	{
		$('#sp2').removeClass('sp_active');
		$('#sp2').addClass('sp_active');
		$('#sp1').removeClass('sp_active');
		$('#sbutton2').removeClass('sp_select_noactive');
		$('#sbutton2').removeClass('sp_select_active');
		$('#sbutton2').addClass('sp_select_active');
		$('#sbutton1').removeClass('sp_select_noactive');
		$('#sbutton1').removeClass('sp_select_active');
		$('#sbutton1').addClass('sp_select_noactive');
	}
}






