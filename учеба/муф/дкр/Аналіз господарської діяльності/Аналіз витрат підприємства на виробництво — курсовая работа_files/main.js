try {document.execCommand('BackgroundImageCache', false, true);} catch(e) {}

$(document).ready(function () {
		
	if (typeof(paperId) != "undefined" && paperId)
	{
		$.ajax({url: '/b_info/?type=center_banner&r='+Math.random()});

		if(!paperLoaded)
		{
			$("a.zip").click(function(){
				if (userAuth != 2) 
				{joinWindow();
				return false;}
			});
			$("a.load").click(function(){
				if (userAuth != 2) 
				{joinWindow();
				return false;}
			});
			/*$("a.open").click(function(){
				if (userAuth != 2) 
				{exchangePaper();
				return false;}
			});*/
			$("div.fileview").mousedown(function(){
				if (userAuth != 2) 
				{joinWindow();
				return false;}
			});
		}
		else
		{
			$("a.zip").click(function(){
				if (userAuth != 2) 
				{SignUpWindow();
				return false;}
			});
			$("a.load").click(function(){
				if (userAuth != 2) 
				{SignUpWindow();
				return false;}
			});
			/*$("a.open").click(function(){
				if (userAuth != 2) 
				{SignUpWindow();
				return false;}
			});*/
			$("div.fileview").mousedown(function(){
				if (userAuth != 2) 
				{SignUpWindow();
				return false;}
			});
			
			if (userAuth != 2) 
			{SignUpWindow();
			return false;}
			
			
		}
	}
	
});

var signLoading = false;

function SignUpWindow()
{
	init();
	content_ = addSignUpWindow();
	buttons = addNewButton('Зарегистрироваться', 'userSignUp()');
	contentStyle = 'height:154px; padding', '20px 20px 30px 20px';
	addNewWindow(	'Регистрация нового пользователя',
					'Спасибо за добавление новой работы на сайт!',
					'Вам необходимо завести учетную запись пользователя',content_, contentStyle, buttons, 500, 330, false);
	$('#popwindow div#home').css('width', '476px');
	$('div#ajax_div2').css('width', '160px');
	
	$('#popwindow #form_user_signup').ajaxForm({
		beforeSubmit: function(a,f,o) {
			o.dataType = "json";
			$('#ajax_div2').addClass('ajax-loading');
			$('#ajax_div2').html('<span id="loadTitle" style="color:#007BBB">Идет загрузка, подождите &nbsp;&nbsp;&nbsp;&nbsp;</span>');
			$('span#loadTitle').animate({opacity:0},0);
			$('span#loadTitle').animate({opacity:1},1500);
			signLoading = true;
		},
		success: function(data) {
			$('#ajax_div2').removeClass('ajax-loading');
			$('#ajax_div2').html('');
			signLoading = false;
			
			if (data.list[0] == "1")
				{
					window.location.reload(true);
				}
			if (data.list[0] != "1")$('#ajax_div2').html(data.list[1].replace('[n]', '\r\n').replace('[n]', '\r\n'));
		}
	});
	window_open('#auth');
}

var joinLoading = false;

function addJoinWindow()
{
	return '<form id="form_join" action="/ajax/upgrade/" enctype="multipart/form-data" method="post">'+
'<link href="http://student.zoomru.ru/i/student/exchange/css/main.css" rel="stylesheet" />'+
'Введите E-mail и пароль, которые будете использовать для входа на сайт:<br/><br/>'+
'            <table id="work">'+
'              <tr>'+
'                <td style="width:90px" class="left">E-mail:</td>'+
'                <td class="right"><input style="width:220px" name="mail" type="text" value="" /></td>'+
'              </tr>'+
'              <tr>'+
'                <td style="width:90px" class="left">Пароль:</td>'+
'                <td class="right"><input style="width:220px" name="password" type="password" value="" /></td>'+
'              </tr>'+
'            </table><br/><br/>'+
'Заведение учетной записи необходимо для дальнейшего доступа к сайту с любого компьютера!'+
'                 </form>';	
}

function isAgree_()
{
var imgProps=$('#join_agree').attr('checked');
if (imgProps) {$("#isAgreeBox").hide(); return true} else {alert('Вы должны принять условия оферты');return false;}
}

function joinWindow()
{
	init();
	content_ = addJoinWindow();
	buttons = addNewButton('Зарегистрироваться', 'userJoin()')+'<div style="margin-left: 5px;float: right;width: 207px;padding: 9px 0px;" id="isAgreeBox"><input name="join_agree" style="width:14px; height:14px;" type="checkbox" name="agree" value="1" id="join_agree"><label id="join_agree_label"> Согласен с условиями </label><a target="_blank" href="/oferta.html">оферты</a></div>';
	contentStyle = 'height:154px; padding', '20px 20px 30px 20px';
	addNewWindow(	'Регистрация нового пользователя',
					'Зарегистрируйтесь сейчас!',
					'Просмотр и скачивание документов docx, doc, xlsx, xsl, ppt, pptx, rtf, odt, pdf и т.д. доступно только для зарегистрированных пользователей',
					content_, contentStyle, buttons, 520, 330, false);
	$('#popwindow div#home').css('width', '496px');
	$('div#ajax_div2').css('width', '180px');
	$('#popwindow div#top').css('padding', '10px 18px 8px');
	
	$('#popwindow #form_join').ajaxForm({
		beforeSubmit: function(a,f,o) {
			o.dataType = "json";
			$('#ajax_div2').addClass('ajax-loading');
			$('#ajax_div2').html('<span id="loadTitle" style="color:#007BBB">Идет загрузка, подождите &nbsp;&nbsp;&nbsp;&nbsp;</span>');
			$('span#loadTitle').animate({opacity:0},0);
			$('span#loadTitle').animate({opacity:1},1500);
			joinLoading = true;
		},
		success: function(data) {
			$('#ajax_div2').removeClass('ajax-loading');
			$('#ajax_div2').html('');
			joinLoading = false;
			
			if (data.list[0] == "1")
				{
					window.location.reload(true);
				}
			if (data.list[0] != "1")$('#ajax_div2').html(data.list[1].replace('[n]', '\r\n').replace('[n]', '\r\n'));
		}
	});
	window_open('#auth');
	$('#join_agree_label').click(function(){$('input[name="join_agree"]').attr("checked", "checked");});
}

function userJoin()
{
if (!joinLoading && isAgree_()) $('#form_join').trigger('submit');
}

function userSignUp()
{
if (!signLoading) $('#form_user_signup').trigger('submit');
}

function addSignUpWindow()
{
	return '<form id="form_user_signup" action="/ajax/userSignUp/" enctype="multipart/form-data" method="post">'+
'<link href="http://student.zoomru.ru/i/student/exchange/css/main.css" rel="stylesheet" />'+
'Введите E-mail и пароль, которые будете использовать для входа на сайт:<br/><br/>'+
'            <table id="work">'+
'              <tr>'+
'                <td style="width:90px" class="left">E-mail:</td>'+
'                <td class="right"><input style="width:220px" name="mail" type="text" value="" /></td>'+
'              </tr>'+
'              <tr>'+
'                <td style="width:90px" class="left">Пароль:</td>'+
'                <td class="right"><input style="width:220px" name="password" type="password" value="" /></td>'+
'              </tr>'+
'            </table><br/><br/>'+
'Заведение учетной записи необходимо для дальнейшего доступа к сайту с любого компьютера!'+
'                 </form>';	
}




function loginIn()
{
$('#form_user_login').trigger('submit');
}

function login()
{
	content_ = userLogin();
	buttons = addNewButton('Войти', 'loginIn()');
	contentStyle = 'height:154px';
	addNewWindow(	'Авторизация',
					'Для входа используйте ваш E-mail адрес и пароль, который вы указали при регистрации*',
					'',content_, contentStyle, buttons, 500, 330, false);
	$('#popwindow div#home').css('width', '476px');
	$('div#ajax_div2').css('width', '225px');
	init();
	window_open('#auth');
	
	$('#form_user_login').ajaxForm({
			beforeSubmit: function(a,f,o) {
				o.dataType = "json";
				$('#ajax_div2').addClass('ajax-loading');
				$('#ajax_div2').html('<span id="loadTitle" style="color:#007BBB">Идет загрузка, подождите &nbsp;&nbsp;&nbsp;&nbsp;</span>');
				$('span#loadTitle').animate({opacity:0},0);
				$('span#loadTitle').animate({opacity:1},1500);
			},
			success: function(data) {
				$('#ajax_div2').removeClass('ajax-loading');
				$('#ajax_div2').html('');
				
				if (data.list[0] == "1")
					{
						window.location.reload(true);
					}
				if (data.list[0] != "1")$('#ajax_div2').html(data.list[1].replace('[n]', '\r\n').replace('[n]', '\r\n'));
			}
		});
	
}

function userLogin()
{
	return '<form id="form_user_login" action="/ajax/userAuth/?auth=true" enctype="multipart/form-data" method="post">'+
'<link href="http://student.zoomru.ru/i/student/exchange/css/main.css" rel="stylesheet" />'+
'            <table id="work">'+
'              <tr>'+
'                <td style="width:70px" class="left">E-mail:</td>'+
'                <td class="right"><input style="width:220px" name="mail" type="text" value="" /></td>'+
'              </tr>'+
'              <tr>'+
'                <td style="width:70px" class="left">Пароль:</td>'+
'                <td class="right"><input style="width:220px" name="password" type="password" value="" />&nbsp;&nbsp;<a style="line-height:25px; font-size:11px; color:#666666" href="/forgot.html">забыли пароль?</a></td>'+
'              </tr>'+
'            </table><br/><br/><br/><br/>'+
'Если у вас возникают трудности со входом &mdash; <a target="_blank" href="/feedback.html">Техническая поддержка</a><br/>* Регистрация будет предложена при скачивании первой работы'+
'                 </form>';	
}

function logout()
{
	$.get("/ajax/userAuth/",
      {"logout":"true"},
      function(returned_data)
          {
          	window.location.reload(true);
          });
}

function addNewButton(title, script)
{
	return '<a class="button button_blue" href="javascript:void(0)" onclick="'+script+'">'+title+'</a>';	
}

function addNewWindow(title, title2, about, content, contentStyle, buttons, width, height, login_in)
{
login_in_code = '';
	
if (login_in) login_in_code = '<div class="user_tool" style="margin: 9px 0px 0px 20px; float: left;"><img src="http://student.zoomru.ru/i/all/i/user_top.gif" style="margin-right: 5px;"><a href="javascript:login()">войти</a></div>';
	
$('#popwindow').html(''+
'	<div style="width:'+width+'px" id="auth" class="window">'+
'<div style="padding:10px; background:#d9e4ea">'+
'	<div style="border:1px solid #c2c2c2; padding:1px; background:#FFFFFF">'+
'		<div style="font-weight:bold; height:23px; padding:7px 8px 0px 18px; background:#007bbb; border-bottom:1px solid #FFFFFF; color:#FFFFFF; font-family:Arial, Helvetica, sans-serif; font-size:14px">'+
'			<a title="Закрыть" style="float:right; margin-top:3px; background:url(\'http://student.zoomru.ru/i/student/exchange/i/close.gif\') no-repeat; display:block; height:11px; width:11px;" href="javascript:void(0)" onclick="window_close()"></a>'+title+
'        </div>'+
'<link href="http://student.zoomru.ru/i/student/exchange/css/main.css" rel="stylesheet" />'+
'<div style="height:'+height+'px" id="home">'+
'	<div style="height:50px; margin:0px; padding:18px 18px 0;" id="top">'+
'    	<span>'+title2+'</span>'+about+
'    </div>'+
'    <div id="content" style="'+contentStyle+'">'+content+
'    </div>'+
'    <div style="height:30px; margin:0px; padding:12px 23px 12px 0;" id="footer">'+login_in_code+
'        <a class="button" href="javascript:void(0)" onclick="window_close()">Отмена</a>'+buttons+
'        <div id="ajax_div2"></div><div style="clear:right"></div>'+
'    </div>'+
'</div>'+
'    </div>'+
'</div>'+
'	</div>'+
'');
}


/*=====POP_WINDOW=====*/
var opacity = 0;
var active_div_open = '';
var el = '';
var popwindow = '';
var h = 0;

function init()
{ 
	opacity = 0.85;
	active_div_open = '';
	el = $('#divopacity');
	popwindow = $('#popwindow');
	h = $(document).height()+'px';

	el.fadeTo(0, 0);
	if(/MSIE (5\.5|6).+Win/.test(navigator.userAgent))	{} else {popwindow.fadeTo(0, 0)}
	window.onresize=function()
		{
			h = $(document).height()+'px';
			el.css({height: h});
		}
	el.css({position: 'absolute', left: '0px', top: '0px', height: h, width: '100%', zIndex: '100', 'visibility': 'visible', 'display': 'block'});
	
}

function window_close()
	{
		el.fadeTo("slow", 0);
		if(/MSIE (5\.5|6).+Win/.test(navigator.userAgent))	{} else {popwindow.fadeTo("slow", 0)}
		$(active_div_open).css({'display': 'none', 'visibility': 'hidden'});
		$(el).css({'display': 'none', 'visibility': 'hidden'});
		popwindow.css({'z-index': '1', 'visibility': 'hidden'});
		return false;
	}
	
function window_open(div_open)
{
	el.fadeTo(850, opacity);
	active_div_open = div_open;
	$(div_open).css({'display': 'block', 'visibility': 'visible'});
	
	var w_ = $(popwindow).width();
	w_ = -Number(w_/2)+'px';
	var h_ = $(popwindow).height();
	h_ = -Number(h_/2)+'px';
	if(/MSIE (5\.5|6).+Win/.test(navigator.userAgent))
	{
	popwindow.css({'margin-left': w_, 'margin-top': h_, 'z-index': '101', 'visibility': 'visible'});
	}
	else
	{
	popwindow.fadeTo("slow", 1);
	popwindow.css({'margin-left': w_, 'left': '50%', 'margin-top': h_, 'top': '50%', 'z-index': '101', 'visibility': 'visible'});
	}
	
	return false;
}

/*=====/POP_WINDOW=====*/

function exchangePaper()
{
	//paper_otype = false;// added 13/12/2012
	if (paper_otype) 
	{
		SelectPackage(true);
	}
	else
	{
		init();
		oferta_type = 1;
		//oferta_accepted = true;// added 13/12/2012
		oferta();
		//oferta_accepted = false;// added 13/12/2012
	}
}

function exchangePaper_()
{
	init();
	addExchangeWindow();
	window_open('#auth');
	//$('#ajax_div2').html('<span style="color:#888888; float:left; display:block">Нажимая "Сохранить" вы соглашаетесь с условиями <a style="color:#888888" target="_blank" href="/oferta.html">Оферты</a></span>'); // added 13/12/2012
}

function addExchangeWindow_()
{
$('#popwindow').html(''+
'	<div style="width:704px" id="auth" class="window">'+
'<div style="padding:10px; background:#d9e4ea">'+
'	<div style="border:1px solid #c2c2c2; padding:1px; background:#FFFFFF">'+
'		<div style="font-weight:bold; height:23px; padding:7px 8px 0px 18px; background:#007bbb; border-bottom:1px solid #FFFFFF; color:#FFFFFF; font-family:Arial, Helvetica, sans-serif; font-size:14px">'+
'			<a title="Закрыть" style="float:right; margin-top:3px; background:url(\'http://student.zoomru.ru/i/student/exchange/i/close.gif\') no-repeat; display:block; height:11px; width:11px;" href="javascript:void(0)" onclick="window_close()"></a>'+
'			Скачивание работы'+
'        </div>'+
'<link href="http://student.zoomru.ru/i/student/exchange/css/main.css" rel="stylesheet" />'+
'<div style="height:430px" id="home">'+
'	<div id="topExchange">'+
'    	<span>Проголосуй!</span>'+
'		После чего получишь эту работу бесплатно!'+
'    </div>'+
'    <div id="contentExchange">'+
'    	<h2><a target="_blank" href="/golden.htm">Проголосовать!</a></h2>'+
'    </div>'+
'    <div style="height:30px" id="footerExchange">'+
'        <a class="button" href="javascript:void(0)" onclick="window_close()">Отмена</a>'+
'        <div id="ajax_div2"></div><div style="clear:right"></div>'+
'    </div>'+
'</div>'+
'    </div>'+
'</div>'+
'	</div>'+
'');
}

function addExchangeWindow()
{
$('#popwindow').html(''+
'	<div style="width:704px" id="auth" class="window">'+
'<div style="padding:10px; background:#d9e4ea">'+
'	<div style="border:1px solid #c2c2c2; padding:1px; background:#FFFFFF">'+
'		<div style="font-weight:bold; height:23px; padding:7px 8px 0px 18px; background:#007bbb; border-bottom:1px solid #FFFFFF; color:#FFFFFF; font-family:Arial, Helvetica, sans-serif; font-size:14px">'+
'			<a title="Закрыть" style="float:right; margin-top:3px; background:url(\'http://student.zoomru.ru/i/student/exchange/i/close.gif\') no-repeat; display:block; height:11px; width:11px;" href="javascript:void(0)" onclick="window_close()"></a>'+
'			Добавление новой работы'+
'        </div>'+
'<link href="http://student.zoomru.ru/i/student/exchange/css/main.css" rel="stylesheet" />'+
'<div style="height:430px" id="home">'+
'	<div id="topExchange">'+
'    	<span>Добавь свою работу!</span>'+
'		После чего получишь любую работу бесплатно!'+
'    </div>'+
'    <div id="contentExchange">'+
'    	<form enctype="multipart/form-data" method="post" action="/ajax/add/?paper_id='+paperId+'" id="form_new_file">'+
'            <table id="work">'+
'              <tr>'+
'                <td class="left">Тема работы:</td>'+
'                <td class="right"><input name="title" type="text" value="" /></td>'+
'              </tr>'+
'              <tr>'+
'                <td class="left">Тип:</td>'+
'                <td class="right">'+
'                	<select name="type">'+
'                		<option value="0">=== Выберите тип работы ===</option>'+
'                		<option value="1">дипломная работа</option>'+
'                		<option value="2">курсовая работа</option>'+
'                		<option value="3">реферат</option>'+
'                		<option value="4">контрольная работа</option>'+
'                		<option value="5">автореферат</option>'+
'                		<option value="8">аттестационная работа</option>'+
'                		<option value="9">биография</option>'+
'                		<option value="10">диссертация</option>'+
'                		<option value="11">доклад</option>'+
'                		<option value="12">задача</option>'+
'                		<option value="13">история болезни</option>'+
'                		<option value="17">курс лекций</option>'+
'                		<option value="18">лабораторная работа</option>'+
'                		<option value="19">лекция</option>'+
'                		<option value="20">магистерская работа</option>'+
'                		<option value="21">методичка</option>'+
'                		<option value="22">монография</option>'+
'                		<option value="23">научная работа</option>'+
'                		<option value="24">отчет по практике</option>'+
'                		<option value="25">практическая работа</option>'+
'                		<option value="26">сочинение</option>'+
'                		<option value="27">статья</option>'+
'                		<option value="28">творческая работа</option>'+
'                		<option value="29">тест</option>'+
'                		<option value="32">шпаргалка</option>'+
'                	</select>'+
'                </td>'+
'              </tr>'+
'              <tr>'+
'                <td class="left">Предмет:</td>'+
'                <td class="right">'+
'                	<select name="section">'+
'                		<option value="0">=== Укажите тематику работы ===</option>'+
'                        <option value="85">Авиация и космонавтика</option>'+
'                        <option value="48">Административное право</option>'+
'                        <option value="66">Анализ финансовой деятельности предприятия</option>'+
'                        <option value="39">Анатомия</option>'+
'                        <option value="80">Антикризисная экономика</option>'+
'                        <option value="57">Арбитражный процесс</option>'+
'                        <option value="20">Архитектура</option>'+
'                        <option value="21">Астрономия</option>'+
'                        <option value="1">Безопасность жизнедеятельности</option>'+
'                        <option value="77">Бизнес-планирование</option>'+
'                        <option value="38">Биология</option>'+
'                        <option value="40">Ботаника</option>'+
'                        <option value="81">Бухгалтерский учёт и аудит</option>'+
'                        <option value="76">Бюджет</option>'+
'                        <option value="63">Бюджетное право</option>'+
'                        <option value="47">Ветеринария</option>'+
'                        <option value="52">Водное право</option>'+
'                        <option value="86">Военная кафедра</option>'+
'                        <option value="15">География</option>'+
'                        <option value="42">Геодезия</option>'+
'                        <option value="43">Геология</option>'+
'                        <option value="27">Геометрия</option>'+
'                        <option value="2">Геополитика</option>'+
'                        <option value="54">Гражданско-процессуальное право</option>'+
'                        <option value="53">Гражданское право</option>'+
'                        <option value="87">Делопроизводство</option>'+
'                        <option value="69">Деньги, кредит, банки</option>'+
'                        <option value="92">Журналистика</option>'+
'                        <option value="51">Земельное право</option>'+
'                        <option value="45">Зоология</option>'+
'                        <option value="70">Инвестиции</option>'+
'                        <option value="71">Инновации</option>'+
'                        <option value="3">Иностранные языки</option>'+
'                        <option value="22">Информатика</option>'+
'                        <option value="4">Искусство</option>'+
'                        <option value="5">Исторические личности</option>'+
'                        <option value="6">История</option>'+
'                        <option value="23">Коммуникации и связь</option>'+
'                        <option value="44">Концепции современного естествознания</option>'+
'                        <option value="93">Косметология</option>'+
'                        <option value="94">Криминалистика</option>'+
'                        <option value="95">Криминология</option>'+
'                        <option value="96">Криптология</option>'+
'                        <option value="97">Кулинария</option>'+
'                        <option value="7">Культурология</option>'+
'                        <option value="9">Литература</option>'+
'                        <option value="26">Логика</option>'+
'                        <option value="98">Логистика</option>'+
'                        <option value="74">Маркетинг</option>'+
'                        <option value="99">Масс-медиа и реклама</option>'+
'                        <option value="24">Математика</option>'+
'                        <option value="46">Медицина</option>'+
'                        <option value="55">Международное право</option>'+
'                        <option value="109">Международные отношения</option>'+
'                        <option value="68">Менеджмент</option>'+
'                        <option value="100">Металлургия</option>'+
'                        <option value="88">Москвоведение</option>'+
'                        <option value="13">Музыка</option>'+
'                        <option value="58">Муниципальное право</option>'+
'                        <option value="82">Налоги</option>'+
'                        <option value="59">Налоговое право</option>'+
'                        <option value="16">Педагогика</option>'+
'                        <option value="101">Полиграфия</option>'+
'                        <option value="17">Политология</option>'+
'                        <option value="65">Политология</option>'+
'                        <option value="102">Предпринимательство</option>'+
'                        <option value="28">Программирование и компьютеры</option>'+
'                        <option value="18">Психология</option>'+
'                        <option value="29">Радиоэлектроника</option>'+
'                        <option value="79">Реинжиниринг</option>'+
'                        <option value="14">Религия и мифология</option>'+
'                        <option value="12">Риторика</option>'+
'                        <option value="41">Сельское хозяйство</option>'+
'                        <option value="8">Социология</option>'+
'                        <option value="30">Статистика</option>'+
'                        <option value="103">Страхование</option>'+
'                        <option value="104">Строительство</option>'+
'                        <option value="31">Схемотехника</option>'+
'                        <option value="105">Таможенная система</option>'+
'                        <option value="64">Теория государства и права</option>'+
'                        <option value="89">Теория организации</option>'+
'                        <option value="32">Теплотехника</option>'+
'                        <option value="33">Технология</option>'+
'                        <option value="90">Товароведение</option>'+
'                        <option value="106">Транспорт</option>'+
'                        <option value="56">Трудовое право</option>'+
'                        <option value="107">Туризм</option>'+
'                        <option value="49">Уголовное право</option>'+
'                        <option value="50">Уголовный  процесс</option>'+
'                        <option value="83">Управление качеством продукции</option>'+
'                        <option value="34">Физика</option>'+
'                        <option value="91">Физкультура и спорт</option>'+
'                        <option value="10">Философия</option>'+
'                        <option value="25">Финансовая математика</option>'+
'                        <option value="60">Финансовое право</option>'+
'                        <option value="67">Финансовый менеджмент</option>'+
'                        <option value="75">Финансы</option>'+
'                        <option value="108">Фотография</option>'+
'                        <option value="35">Химия</option>'+
'                        <option value="61">Хозяйственное право</option>'+
'                        <option value="78">Ценообразование</option>'+
'                        <option value="36">Цифровые устройства</option>'+
'                        <option value="62">Экологическое право</option>'+
'                        <option value="19">Экология</option>'+
'                        <option value="110">Экономика</option>'+
'                        <option value="73">Экономика организации</option>'+
'                        <option value="37">Экономико-математическое моделирование</option>'+
'                        <option value="84">Экономическая география</option>'+
'                        <option value="72">Экономическая теория</option>'+
'                        <option value="11">Этика</option>'+
'                 	</select>'+
'                </td>'+
'              </tr>'+
'              <tr>'+
'                <td class="left">Краткое описание:</td>'+
'                <td class="right"><textarea name="about" cols="" rows=""></textarea></td>'+
'              </tr>'+
'              <tr>'+
'                <td class="left"><div>Файл с работой:</div></td>'+
'                <td class="right"><div id="wrapper"><input id="File1" name="new_file" type="file" /></div></td>'+
'              </tr>'+
'            </table>'+
'            <table style="display:none" id="user">'+
'              <tr>'+
'                <td class="left">Имя:</td>'+
'                <td class="right"><input name="username" type="text" value="" /></td>'+
'              </tr>'+
'              <tr>'+
'                <td class="left">Фамилия:</td>'+
'                <td class="right"><input name="username2" type="text" value="" /></td>'+
'              </tr>'+
'              <tr>'+
'                <td class="left">E-mail:</td>'+
'                <td class="right"><input name="mail" type="text" value="" /></td>'+
'              </tr>'+
'              <tr>'+
'                <td class="left"></td>'+
'                <td class="right">Пожалуйста, указывайте только настоящее Имя, Фамилию и E-mail, иначе вы не сможете получить нужную вам работу!</td>'+
'              </tr>'+
'            </table>'+
'		</form>'+
'    </div>'+
'    <div style="height:30px" id="footerExchange">'+
'        <a class="button" href="javascript:void(0)" onclick="window_close()">Отмена</a>'+
'        <a class="button button_blue" href="javascript:void(0)" onclick="formSubmit()">Сохранить</a>'+
'        <div id="ajax_div2"></div><div style="clear:right"></div>'+
'    </div>'+
'</div>'+
'<script src="http://student.zoomru.ru/i/student/exchange/js/main.js"></script>'+
'    </div>'+
'</div>'+
'	</div>'+
'');
}
