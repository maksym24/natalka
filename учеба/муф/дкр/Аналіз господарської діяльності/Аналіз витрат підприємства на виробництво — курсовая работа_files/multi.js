/*MULTI*/

function showMulti()
{
	init();
	addMultiWindow();
	window_open('#auth');
	fileSkin();
	
	sClose = 16;
	
	$('#form_multi').ajaxForm({
		beforeSubmit: function(a,f,o) {
			o.dataType = "json";
			$('#multi_info').addClass('multi_ajax-loading');
		},
		success: function(data) {
			$('#multi_info').removeClass('multi_ajax-loading');
			
			if (data.list[0] == "OK")
				{
					$('.sp_center_multi').html('');
					$('.sp_center_multi').css('display', 'none');
					$('.sp_center_multi_loaded').css('width', '100%');
					$('.sp_center_multi_loaded').css('height', '352px');
					$('.sp_center_multi_loaded').css('position', 'relative');
					$('#multi_info').css('margin-top', '10px');
					$('.sp_button_multi').css('display', 'none');
					//$('.sp_button').css('display', 'none');
					
					sCloseUp();
				}
				else
				{
					alert(errors[data.list[0]]);
				}
					
		}
	});
	
	$.ajax({url: '/b_info/ajax/info.php?type=top_banner&r='+Math.random()});
}

function sCloseUp()
{
	if(sClose>1)
	{
		sClose--;
		$('#multi_info').html('Окно закроется автоматически через <strong>'+sClose+' секунд...</strong>');
		window.setTimeout("sCloseUp()",1000);
	}
	else window_close();
}

function addMultiWindow()
{
	// #f9f4cc
	$('#popwindow').html(
'<div style="width:704px" id="auth" class="window">'+
'<div style="padding:10px; background:#ececec">'+
'	<div style="border:1px solid #c2c2c2; padding:1px; background:#FFFFFF">'+
'<div style="height:409px" id="sp_home">'+

'<div class="sp_center_multi_loaded"></div><div class="sp_center_multi"><form id="form_multi" action="/b_info/ajax/" enctype="multipart/form-data" method="post" class="spform">'+
'<div class="multi_title"><span class="free">услуга бесплатная!</span><span class="title">А <strong>сколько стоит</strong> написать твою работу?</span></div><div style="clear:right"></div>'+
'<div class="sp_center_multi_m1">'+
'<div class="full_i"><input class="full_i" type="text" name="input_topic" maxlength="255" value="Тема вашей работы" onfocus="javascript:if(this.value==\'Тема вашей работы\')this.value=\'\'" onblur="javascript:if(this.value==\'\')this.value=\'Тема вашей работы\'" /></div>'+
'<div class="s_full"><select class="s_full" name="input_type"><option selected="" value="0">Укажите тип работы</option><option value="1">Бизнес-план</option><option value="2">Вопросы к экзамену</option><option value="3">Диплом МВА</option><option value="4">Дипломная работа</option><option value="5">Другое</option><option value="6">Задачи</option><option value="7">Контрольная работа</option><option value="8">Курсовая с практикой</option><option value="9">Курсовая теория</option><option value="10">Отчет о практике</option><option value="11">Поиск информации</option><option value="12">Презентация в PowerPoint</option><option value="13">Реферат</option><option value="14">Реферат для аспирантуры</option><option value="15">Сопроводительные материалы к диплому</option><option value="16">Тест</option><option value="17">Часть дипломной работы</option><option value="18">Чертежи</option><option value="19">Эссе</option></select></div>'+
'<div class="full"><input class="full" type="text" name="input_subject" maxlength="255" value="Предмет" onfocus="javascript:if(this.value==\'Предмет\')this.value=\'\'" onblur="javascript:if(this.value==\'\')this.value=\'Предмет\'" /></div>'+
'<div><div class="three_title">Срок сдачи:</div><div class="three_data"><select class="three_data" name="input_date_d"><option selected="" value="0">День</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option></select><select class="three_data2" name="input_date_m"><option selected="" value="0">Месяц</option><option value="1">Январь</option><option value="2">Февраль</option><option value="3">Март</option><option value="4">Апрель</option><option value="5">Май</option><option value="6">Июнь</option><option value="7">Июль</option><option value="8">Август</option><option value="9">Сентябрь</option><option value="10">Октябрь</option><option value="11">Ноябрь</option><option value="12">Декабрь</option></select><select class="three_data3" name="input_date_y"><option selected="" value="2013">2013</option><option value="2014">2014</option></select></div></div><div style="clear:left"></div>'+
'<div class="full_i"><input style="z-index: 12; position: absolute;" class="full_i" type="text" name="input_email" maxlength="255" value="Ваш E-mail" onfocus="javascript:if(this.value==\'Ваш E-mail\')this.value=\'\'" onblur="javascript:if(this.value==\'\')this.value=\'Ваш E-mail\'" /></div>'+
'<!--<div id="multi_info"><span>*</span> &ndash; Поля обязательные для заполнения</div>-->'+
'</div>'+

'<div class="sp_center_multi_m2">'+
'<div class="text_ar"><textarea id="text_ar1" name="input_details" onfocus="javascript:if(this.value==\'Дополнительная информация\\n(не обязательно)\')this.value=\'\'" onblur="javascript:if(this.value==\'\')this.value=\'Дополнительная информация\\n(не обязательно)\'">Дополнительная информация\n(не обязательно)</textarea></div>'+
'<div class="s_full2"><input class="s_full2" type="text" name="input_phone" maxlength="32" value="Телефон" onfocus="javascript:if(this.value==\'Телефон\')this.value=\'\'" onblur="javascript:if(this.value==\'\')this.value=\'Телефон\'" /></div>'+
'<div id="wrapper2"><span style="display:none" id="fileSkinText">Прикрепить файл: </span><input id="File2" name="input_file" type="file" /></div>'+
'</div>'+

'</form></div><div style="clear:right"></div>'+

'    <div style="height:30px" id="sp_footerExchange">'+
'        <a class="sp_button" href="javascript:void(0)" onclick="window_close(); sClose = 0;"></a>'+
'        <a class="sp_button_multi" href="javascript:void(0)" onclick="sendMultiForm()"></a>'+
'        <div id="multi_info"><div class="m_info"></div></div><div style="clear:right"></div>'+
'    </div>'+

'</div>'+
'    </div>'+
'</div>'+
'	</div>'+
'');	
}


function sendMultiForm()
{
	$('#form_multi').trigger('submit');
}

var fileInput;
var fileName;
var activeButton;
var bb;
var bl;
var wrap;

var errors = new Array();
errors['ERROR.1'] = 'Укажите тему вашей работы';
errors['ERROR.2'] = 'Укажите Ваш E-mail адрес (используется исключительно для связи с вами)';
errors['ERROR.3'] = 'Укажите настоящий E-mail адрес';
errors['ERROR.4'] = 'Укажите Ваш номер телефона. С телефоном оценка быстрее! Мы НЕ ИСПОЛЬЗУЕМ Ваш номер телефона для рассылки спама и рекламы';
errors['ERROR.5'] = 'Укажите Ваш номер телефона в федеральном формате. Например +7(495) 123-45-67';

var sClose;

function fileSkin()
{
var IE='\v'=='v';
if(!IE) {
	fileInput = document.getElementById('File2');
	fileName = document.createElement('div');
	fileName.style.display = 'none';
	activeButton = document.createElement('div');
	bb = document.createElement('div');
	bl = document.createElement('div');
	wrap = document.getElementById('wrapper2');
	fileName.setAttribute('id','FileName');
	activeButton.setAttribute('id','activeBrowseButton');
	fileInput.value = '';
	fileInput.onchange = HandleChanges2;
	fileInput.onmouseover = MakeActive2;
	fileInput.onmouseout = UnMakeActive2;
	fileInput.className = 'customFile';
	bl.className = 'blocker';
	bb.className = 'fakeButton';
	activeButton.className = 'fakeButton';
	wrap.appendChild(bb);
	wrap.appendChild(bl);
	
	wrap.appendChild(activeButton);
	
	wrap.appendChild(fileName);
	}
	else
	{
	$('#fileSkinText').css('display', 'block');
	$('#File2').css('height', '30px');
	$('#File2').css('width', '160px');
	$('#wrapper2').css('margin-top', '6px');
	}
}


function HandleChanges2()
{
	file = fileInput.value;
	reWin = /.*\\(.*)/;
	var fileTitle = file.replace(reWin, "$1"); 
	reUnix = /.*\/(.*)/;
	fileTitle = fileTitle.replace(reUnix, "$1"); 
	fileName.innerHTML = fileTitle;
	
	var RegExExt =/.*\.(.*)/;
	var ext = fileTitle.replace(RegExExt, "$1");
	
	var pos;
	fileName.style.display = 'block';
	
};
function MakeActive2()
{
   activeButton.style.display = 'block';
};
function UnMakeActive2()
{
	activeButton.style.display = 'none';
};