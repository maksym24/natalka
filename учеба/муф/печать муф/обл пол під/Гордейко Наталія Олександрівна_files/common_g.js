function tuneLinks()
{
 var AArray = document.all.tags("A");
 for (var i = 0; i < AArray.length; i++)
     {
       if ((AArray[i].className=="a-disabled") || (AArray[i].className=="lang-dis") )
                   {
                     AArray[i].href="#";
                   }
     }
}

function openWindow(theURL,winName,features) { 
  if (!features) features="status=yes,toolbar=no,menubar=no,location=no";
  return window.open(theURL,winName,features);
}

function sm(params) { 
  sm_wnd = openWindow("sendmsg.php?"+params,'',"status=yes,toolbar=no,menubar=no,location=no, width=700,height=550");
  sm_wnd.focus();
}

/***
 ****		Flash Detecting (taked from GA compilation)
 ***/

function getFlashVer() {
 var f="-",n=navigator;
 if (n.plugins && n.plugins.length) {
  for (var ii=0;ii<n.plugins.length;ii++) {
   if (n.plugins[ii].name.indexOf('Shockwave Flash')!=-1) {
    f=n.plugins[ii].description.split('Shockwave Flash ')[1];
    break;
   }
  }
 } else if (window.ActiveXObject) {
  for (var ii=12;ii>=2;ii--) {
   try {
    var fl=eval("new ActiveXObject('ShockwaveFlash.ShockwaveFlash."+ii+"');");
    if (fl) { f=ii + '.0'; break; }
   }
   catch(e) {}
  }
 }
 return f;
} 

/***
 ****		Other
 ***/

function array_search(needle, haystack) {
	if (typeof(needle)=='undefined' || typeof(haystack)=='undefined') return false;
	if (typeof(haystack.length)=='undefined' || needle==false) return false;
	for(var i=0; i <= haystack.length-1; i++) if (needle==haystack[i]) return i;
	return -1;	
}
function in_array(needle, haystack) {
	if (typeof(needle)=='undefined' || typeof(haystack)=='undefined') return false;
	if (typeof(haystack.length)=='undefined' || !needle) return false;
	for(var i=0; i <= haystack.length-1; i++) if (needle==haystack[i]) return true;
	return false;	
}

function empty(variable) {
	if (typeof(variable)=='undefined') return true;
	if (variable=='' || variable==0 || variable=='0' || !variable) return true;
	return false;	
}




/***
 ****		General (taked from PMA compilation)
 ***/
 
function getElement(e,f){
    f=(f)?f:self;
	if (document.getElementById) return f.document.getElementById(e);
    if (document.layers){
        f=(f)?f:self;
        if(f.document.layers[e]) {
            return f.document.layers[e];
        }
        for(W=0;i<f.document.layers.length;W++) {
            return(getElement(e,f.document.layers[W]));
        }
    }
    if(document.all) {
        return f.document.all[e];
    }
    return false;
//    return f.document.getElementById(e);
} 

/***
 ****		Forms General Set (taked from PMA compilation)
 ***/

/**
 * Check if a form's element is empty
 * should be
 *
 * @param   object   the form
 * @param   string   the name of the form field to put the focus on
 *
 * @return  boolean  whether the form field is empty or not
 */ 
function emptyCheckTheField(theForm, theFieldName)
{
    var isEmpty  = 1;
    var theField = theForm.elements[theFieldName];
    // Whether the replace function (js1.2) is supported or not
    var isRegExp = (typeof(theField.value.replace) != 'undefined');

    if (!isRegExp) {
        isEmpty      = (theField.value == '') ? 1 : 0;
    } else {
        var space_re = new RegExp('\\s+');
        isEmpty      = (theField.value.replace(space_re, '') == '') ? 1 : 0;
    }

    return isEmpty;
} // end of the 'emptyCheckTheField()' function 


/**
 * Ensures a value submitted in a form is numeric and is in a range
 *
 * @param   object   the form
 * @param   string   the name of the form field to check
 * @param   integer  the minimum authorized value
 * @param   integer  the maximum authorized value
 *
 * @return  boolean  whether a valid number has been submitted or not
 */
function checkFormElementInRange(theForm, theFieldName, message, min, max)
{
    var theField         = theForm.elements[theFieldName];
    var val              = parseInt(theField.value);

    if (typeof(min) == 'undefined') {
        min = 0;
    }
    if (typeof(max) == 'undefined') {
        max = Number.MAX_VALUE;
    }

    // It's not a number
    if (isNaN(val)) {
        theField.select();
        alert("Not a number!");
        theField.focus();
        return false;
    }
    // It's a number but it is not between min and max
    else if (val < min || val > max) {
        theField.select();
		if (!message) message = "Number '%d' is out of range";
        alert(message.replace('%d', val));
        theField.focus();
        return false;
    }
    // It's a valid number
    else {
        theField.value = val;
    }
    return true;

} // end of the 'checkFormElementInRange()' function 



/**
 * marks all rows and selects its first checkbox inside the given element
 * the given element is usaly a table or a div containing the table or tables
 *
 * @param    container    DOM element
 */
var marked_row = new Array;

function markAllRows( container_id ) {
    var rows = document.getElementById(container_id).getElementsByTagName('tr');
    var unique_id;
    var checkbox;

    for ( var i = 0; i < rows.length; i++ ) {

        checkbox = rows[i].getElementsByTagName( 'input' )[0];

        if ( checkbox && checkbox.type == 'checkbox' ) {
            unique_id = checkbox.name + checkbox.value;
            if ( checkbox.disabled == false ) {
                checkbox.checked = true;
                if ( typeof(marked_row[unique_id]) == 'undefined' || !marked_row[unique_id] ) {
                    rows[i].className += ' marked';
                    marked_row[unique_id] = true;
                }
            }
        }
    }
    return true;
}

/**
 * marks all rows and selects its first checkbox inside the given element
 * the given element is usaly a table or a div containing the table or tables
 *
 * @param    container    DOM element
 */
function unMarkAllRows( container_id ) {
    var rows = document.getElementById(container_id).getElementsByTagName('tr');
    var unique_id;
    var checkbox;

    for ( var i = 0; i < rows.length; i++ ) {

        checkbox = rows[i].getElementsByTagName( 'input' )[0];

        if ( checkbox && checkbox.type == 'checkbox' ) {
            unique_id = checkbox.name + checkbox.value;
            checkbox.checked = false;
            rows[i].className = rows[i].className.replace(' marked', '');
            marked_row[unique_id] = false;
        }
    }

    return true;
}

 
 
function markRowsInit() {
    // for every table row ...
    var rows = document.getElementsByTagName('tr');
    for ( var i = 0; i < rows.length; i++ ) {
        // ... with the class 'odd' or 'even' ...
        if ( 'odd' != rows[i].className.substr(0,3) && 'even' != rows[i].className.substr(0,4) ) {
            continue;
        }
        // ... add event listeners ...
        // ... to highlight the row on mouseover ...
        if ( navigator.appName == 'Microsoft Internet Explorer' ) {
            // but only for IE, other browsers are handled by :hover in css
            rows[i].onmouseover = function() {
                this.className += ' hover';
            }
            rows[i].onmouseout = function() {
                this.className = this.className.replace( ' hover', '' );
            }
        }
        // Do not set click events if not wanted
        if (rows[i].className.search(/noclick/) != -1) {
            continue;
        }
        // ... and to mark the row on click ...
        rows[i].onmousedown = function() {
            var unique_id;
            var checkbox;

            checkbox = this.getElementsByTagName( 'input' )[0];
            if ( checkbox && checkbox.type == 'checkbox' ) {
                unique_id = checkbox.name + checkbox.value;
            } else if ( this.id.length > 0 ) {
                unique_id = this.id;
            } else {
                return;
            }

            if ( typeof(marked_row[unique_id]) == 'undefined' || !marked_row[unique_id] ) {
                marked_row[unique_id] = true;
            } else {
                marked_row[unique_id] = false;
            }

            if ( marked_row[unique_id] ) {
                this.className += ' marked';
            } else {
                this.className = this.className.replace(' marked', '');
            }

            if ( checkbox && checkbox.disabled == false ) {
                checkbox.checked = marked_row[unique_id];
            }
        }

        // ... and disable label ...
        var labeltag = rows[i].getElementsByTagName('label')[0];
        if ( labeltag ) {
            labeltag.onclick = function() {
                return false;
            }
        }
        // .. and checkbox clicks
        var checkbox = rows[i].getElementsByTagName('input')[0];
        if ( checkbox ) {
            checkbox.onclick = function() {
                // opera does not recognize return false;
                this.checked = ! this.checked;
            }
        }
    }
}
window.onload=markRowsInit;  



/**
 * Checks/unchecks all checkbox in given conainer (f.e. a form, fieldset or div)
 *
 * @param   string   container_id  the container id
 * @param   boolean  state         new value for checkbox (true or false)
 * @return  boolean  always true
 */
function setCheckboxes( container_id, state ) {
    var checkboxes = document.getElementById(container_id).getElementsByTagName('input');
	if (state) { state=true; } else { state=false; }

    for ( var i = 0; i < checkboxes.length; i++ ) {
        if ( checkboxes[i].type == 'checkbox' ) {
            checkboxes[i].checked = state;
        }
    }

    return true;
} // end of the 'setCheckboxes()' function 


/**
  * Checks/unchecks all options of a <select> element
  *
  * @param   string   the form name
  * @param   string   the element name
  * @param   boolean  whether to check or to uncheck the element
  *
  * @return  boolean  always true
  */
function setSelectOptions(the_form, the_select, do_check)
{
    var selectObject = document.forms[the_form].elements[the_select];
    var selectCount  = selectObject.length;
	if (do_check) { do_check=true; } else { do_check=false; }

    for (var i = 0; i < selectCount; i++) {
        selectObject.options[i].selected = do_check;
    } // end for

    return true;
} // end of the 'setSelectOptions()' function


/**
 *** 	Custom Form functions
 **/

function processOptionSelect(selObj){
	if (selObj.selectedIndex) { // check - is this "Select" object 
		var value = selObj.options[selObj.selectedIndex].value;
		process = true;
		if (value=="delete") process = confirm("Do you really want to "+value+ " selected records?");
		if (process) {
			selObj.form.submit();
			return true;
		} 
	}
	return false;
}



/***
 ****		Array f-ns
 ***/

function show_array(arr) {
	var str = '';
//	str = this +', ';	alert(this);
	for (var k in arr) {
//		if (  !in_array(k, arrPrototypeReserved) )// && typeof(this[k]) != "undefined"
			str = str + k + "=>" + arr[k] + ', ';
	}
//		str = str +',' + k;
	if (str.length>0) str = str.substring(0,str.length-2);
	alert(str);
}

function init_arr_from_str(str) {
	if (!empty(str)) {
		var tmp_arr = str.split(",");
		var out_arr = new Array();
		for (var k in tmp_arr) if (!empty(tmp_arr[k])) {
			var tmp_arr2 = tmp_arr[k].split(":");
			out_arr[tmp_arr2[0]] = tmp_arr2[1];
		}
		return out_arr;
//		show_array(out_arr);
	}
	return false;
}



/***
 ***		Form Handling
 ***/


function el_value(el) { 	// takes form element value
//	alert(el.type);
	var ret_val = false;
	if (!empty(el.type)) {
		var tmp = el.type;
		if (tmp.indexOf('select')>-1) {
//			alert(el.id + el.selectedIndex);
			if (el.selectedIndex != -1)	return el.options[el.selectedIndex].value;
//			return ret_val;
		}
	}
	if (typeof(el.value)!='undefined') return el.value;
	return ret_val;
}

//var value = selObj.options[selObj.selectedIndex].value; 



/***
 ***		Events Functions
 ***/

function addEvent(elm, evType, fn, useCapture) {
	useCapture = useCapture || false;
	if (elm.addEventListener) {
		elm.addEventListener(evType, fn, useCapture);
		return true;
	}
	else if (elm.attachEvent) {
		var r = elm.attachEvent('on' + evType, fn);
		return r;
	}
	else {
		elm['on' + evType] = fn;
	}
}


/***
 ***		Prototype Advancing
 ***/

/*
if (typeof Node != 'undefined') {
	if (typeof Node.children == 'undefined') {
	eval('Node.prototype.children getter = function() {return this.childNodes;}');
	}
}
*/

/*
*/


if (typeof HTMLUListElement != 'undefined') {
	if (typeof HTMLUListElement.children == 'undefined' && HTMLElement.prototype.__defineGetter__) {
//	eval('Node.prototype.children getter = function() {return this.childNodes;}');
		HTMLUListElement.prototype.__defineGetter__("children", function () {
//HTMLUListElement.prototype.children getter = function() {	
			var elements_arr = this.childNodes;
			var ret_elements_arr = new Array();
			var j = 0;
			var str='';
			for (var i = 0; i < elements_arr.length; i++) {
				var curr_el = elements_arr[i];
//				alert(typeof(curr_el));				break;
				var el_type = curr_el.toString();
//				str += ' '+el_type+' ';
				if (el_type.indexOf('HTMLLIElement')>=0) { // object HTMLLIElement
					ret_elements_arr[j] = curr_el;
					j++;
				}
			}
			return ret_elements_arr;
		})
	}
}


var _emptyTags = {
   "IMG":   true,
   "BR":    true,
   "INPUT": true,
   "META":  true,
   "LINK":  true,
   "PARAM": true,
   "HR":    true
};
if (typeof HTMLElement != 'undefined') {
	if (typeof HTMLElement.outerHTML == 'undefined' && HTMLElement.prototype.__defineGetter__) {

		HTMLElement.prototype.__defineGetter__("outerHTML", function () {
		   var attrs = this.attributes;
		   var str = "<" + this.tagName;
		   for (var i = 0; i < attrs.length; i++)
			  str += " " + attrs[i].name + "=\"" + attrs[i].value + "\"";
		
		   if (_emptyTags[this.tagName])
			  return str + ">";
		
		   return str + ">" + this.innerHTML + "</" + this.tagName + ">";
		});

	}
}
/*
*/
