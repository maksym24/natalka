        var timerMulti = 0;
        var jg = new jsGraphics("myCanvas");
        var min_count = 0;
        var total = 0;
        var left = 0;

        function jgDraw(Xc, Yc, size, deg)
        {
              jg.clear();
              if(deg < 180)
              {
                deg_arc_fill = 360+90-deg;
                deg_rad = (deg+270)*Math.PI/180;
              }
              else if(deg >= 180)
              {
                deg_arc_fill = 360+90-deg;
                deg_rad = (deg-90)*Math.PI/180;
              }

              if(deg >= 0 && deg < 234)
                  jg.setColor("#c2ff67");             //green
              else if(deg >= 234 && deg < 252)
                jg.setColor("#ffff80");             //yellow
              else if(deg >= 252 && deg < 360)
                jg.setColor("#ff0000");            //red
              else if(deg >= 360)
                jg.setColor("#fff00");            //white

              jg.fillArc(Xc, Yc, size, size,90,deg_arc_fill)   //under gif

              var stroke = 3;
              var c = size / 2;
              var r = (c*Math.SQRT1_2);
              var rY = c*Math.sin(deg_rad);
              var rX = c*Math.cos(deg_rad);

              jg.setColor("#000000");
              jg.setStroke(stroke);
              jg.drawPolyline( new Array(c, c+rX), new Array(c, c+rY) );
              jg.setStroke(1);
              jg.paint();
        }
	

        function Draw_timer()
        {
                left = total-min_count;

                if(left <= 0)
                {
                    clearInterval(timerMulti);                    
                    jgDraw(2,2,190,360);
                    alert('��� ��� ������!');
                    document.form1.submit();
                }

                deg = 360 - (360/total)*left;
                jgDraw(2,2,190,deg);
                min_count++;
                if(left % 60 == 0)
                {
                    $('.time_left').text(left/60);
                }
            }

       	$('.go').click(function()
	{
            //clearInterval(timerMulti);
            min_count = total-left;
            Draw_timer();
            timerMulti = window.setInterval("Draw_timer();", 1000);
	});

        function getClientWidth()
        {
            return document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientWidth:document.body.clientWidth;
        }

        $(document).ready(function()
	{
            
            total = $('input[id=timer]').attr("value");
            if(total > 0)
            {
                width = getClientWidth()/2;
                $(function() {$('#clock_widget').floating_panel({'fromCenter': width,'fromTop': 10,'minTop': 500,'location': 'left'});});
                left = total;
                min_count = total-left;
                Draw_timer();
                timerMulti = window.setInterval("Draw_timer();", 1000);                                
                $('#clock_widget').show();
                $(function(){$(document).pngFix();});                

            }
	});
