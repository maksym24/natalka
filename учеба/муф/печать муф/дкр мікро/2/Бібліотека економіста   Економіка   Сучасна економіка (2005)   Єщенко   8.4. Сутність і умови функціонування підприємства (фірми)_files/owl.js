$(document).ready(function() {
	$('.bt').bt({
		contentSelector: "$('div.owl').html()",
		width: '500px',
		positions: ['top', 'right'],
		centerPointX: .1,
		strokeStyle: '#666',
		fill: '#ee1',
		spikeGirth: 20,
		cornerRadius: 10,
		shadow: true,
		shadowOffsetX: 3,
		shadowOffsetY: 3,
		shadowBlur: 10,
		shadowColor: 'rgba(0,0,0,.6)',
		shadowOverlap: false,
		hoverIntentOpts: { interval: 300, timeout: 2000 }
	});
});